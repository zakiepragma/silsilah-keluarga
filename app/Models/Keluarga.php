<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keluarga extends Model
{
    use HasFactory;

    protected $table = 'keluarga';

    protected $fillable = ['nama', 'jenis_kelamin', 'ayah_id'];

    public function ayah()
    {
        return $this->belongsTo(Keluarga::class, 'ayah_id');
    }

    public function anak()
    {
        return $this->hasMany(Keluarga::class, 'ayah_id');
    }
}
