<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreKeluargaRequest;
use App\Http\Requests\UpdateKeluargaRequest;
use App\Models\Keluarga;
use Illuminate\Http\Request;

class KeluargaController extends Controller
{
    // /**
    //  * Remove the specified resource from storage.
    //  */
    // public function destroy(Keluarga $keluarga)
    // {
    //     //
    // }

    // public function index()
    // {
    //     $data = Keluarga::all();
    //     return response()->json($data);
    // }

    // public function store(Request $request)
    // {
    //     $data = new Keluarga;
    //     $data->nama = $request->input('nama');
    //     $data->jenis_kelamin = $request->input('jenis_kelamin');
    //     $data->ayah_id = $request->input('ayah_id');
    //     $data->save();
    //     return response()->json(['message' => 'Data berhasil ditambahkan']);
    // }

    // public function show($id)
    // {
    //     $data = Keluarga::find($id);
    //     return response()->json($data);
    // }

    // public function update(Request $request, $id)
    // {
    //     $data = Keluarga::find($id);
    //     $data->nama = $request->input('nama');
    //     $data->jenis_kelamin = $request->input('jenis_kelamin');
    //     $data->ayah_id = $request->input('ayah_id');
    //     $data->save();
    //     return response()->json(['message' => 'Data berhasil diupdate']);
    // }

    // public function destroy($id)
    // {
    //     $data = Keluarga::find($id);
    //     if (!$data) {
    //         return response()->json(['message' => 'Data tidak ditemukan']);
    //     }
    //     $data->delete();
    //     return response()->json(['message' => 'Data berhasil dihapus']);
    // }

    public function index()
    {
        return Keluarga::with('ayah', 'anak')->get();
    }

    public function show(Keluarga $keluarga)
    {
        return $keluarga;
    }

    public function store(Request $request)
    {
        $keluarga = Keluarga::create($request->all());

        return response()->json(['message' => 'Data berhasil ditambahkan', $keluarga]);
    }

    public function update(Request $request, Keluarga $keluarga)
    {
        $keluarga->update($request->all());

        return response()->json(['message' => 'Data berhasil diupdate']);
    }

    public function destroy(Keluarga $keluarga)
    {
        $keluarga->delete();

        return response()->json(['message' => 'Data berhasil dihapus']);
    }
}
